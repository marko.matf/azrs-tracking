#include "ship.h"
#include "field.h"

Ship::Ship (int xId, int xSize) : Field ()
{
	size = xSize;
	this->setId (xId);
}

Ship::~Ship () = default;

auto
Ship::getSize () -> int
{
	return size;
}
void
Ship::setSize (int x)
{
	size = x;
}
