#include "mainwindow.h"
#include "field.h"
#include "randomsea.h"
#include "sea.h"
#include "ship.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QRegExp>
#include <QString>
#include <QTextStream>
#include <stdlib.h>

int i = 1;

// QToolButton* makeButtons();

MainWindow::MainWindow (QWidget* parent)
    : QMainWindow (parent), ui (new Ui::MainWindow)
{
	// Mainwindow setup ----> ovde se namesta sve pre pocetka igre
	ui->setupUi (this);
	// Unos koordinata 0 - 7
	ui->lineEdit_x1->setValidator (new QIntValidator (0, 7, this));
	ui->lineEdit_y1->setValidator (new QIntValidator (0, 7, this));
	ui->lineEdit_x2->setValidator (new QIntValidator (0, 7, this));
	ui->lineEdit_y2->setValidator (new QIntValidator (0, 7, this));
	ui->lineEditFire_x1->setValidator (new QIntValidator (0, 7, this));
	ui->lineEditFire_y1->setValidator (new QIntValidator (0, 7, this));

	printSeaHost (seaHome);

	ui->gridLayout->setEnabled (true);
	clientID = 0;
	// seaHome = MainWindow::createSea();

	socket = new QTcpSocket (this);

	// This is how we tell Qt to call our readyRead() and connected()
	// functions when the socket has text ready to be read, and is done
	// connecting to the server (respectively):
	connect (socket, SIGNAL (readyRead ()), this, SLOT (readyRead ()));
	connect (socket, SIGNAL (connected ()), this, SLOT (connected ()));

	// Ucitavanje mora iz fajla layouts.dat i upisivanje u fleetLayouts
	readSeaFromFile ();
}


void
MainWindow::deleteSea ()
{
    // Brisanje brodova
    seaHome.setShipsLeft (0, 3);
    seaHome.setShipsLeft (1, 3);
    seaHome.setShipsLeft (2, 2);
    seaHome.setShipsLeft (3, 1);

    // Duzine brodova su 1,2,3,4
    // ovde su zamenjene sa 0,1,2,3
    // zbog lakseg rukovanja

    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++)
            seaHome.getField (i, j).setId (0);
}

int id = 1;

// Vraca string koji nam treba da bi postavili
// odgovarajucu boju dugmeta
QString
fieldColor (Field xField)
{

	if (xField.isHit ()) {
		if (xField.getId () != 0)
			return "background-color: rgb(255, 0, 0)";
		else
			return "background-color: rgb(40, 0, 120)";
	} else
		return "background-color: rgb(0, 85, 255)";
}

// Vraca string koji nam treba da bi postavili
// odgovarajucu boju dugmeta
QString
fieldColorHost (Field xField)
{

	if (xField.isHit ()) {
		if (xField.getId () != 0)
			return "background-color: rgb(255, 0, 0)";
		else
			return "background-color: rgb(40, 0, 120)";
	} else {
		if (xField.getId () != 0)
			return "background-color: rgb(0, 255, 0)";
		else
			return "background-color: rgb(0, 85, 255)";
	}
}

void
MainWindow::printSeaHostText (Sea& xSea)
{
	ui->textEdit->setText ("");
	for (unsigned int k = 0; k < 8; k++) {
		for (unsigned int j = 0; j < 8; j++) {

			if (xSea.getField (k, j).isHit ()) {
				ui->textEdit->insertPlainText ("x  ");
			} else if (xSea.getField (k, j).getId ()
				   != 0) // Provera da li je dato Polje zauzeto
					 // Brodom
			{
				// SetConsoleTextAttribute(h, FOREGROUND_GREEN
				// |
				//      FOREGROUND_INTENSITY);

				ui->textEdit->insertPlainText ("q  ");
			} else {
				// SetConsoleTextAttribute(h, FOREGROUND_RED |
				//      FOREGROUND_INTENSITY);
				ui->textEdit->insertPlainText ("o  ");
			}
		}
		// cout << endl;
		ui->textEdit->append ("");
	}
}

//
void
MainWindow::printSeaHost (Sea& xSea)
{

	// Nesto bitno

	{

		ui->field01->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 0)));
		ui->field02->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 1)));
		ui->field03->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 2)));
		ui->field04->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 3)));
		ui->field05->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 4)));
		ui->field06->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 5)));
		ui->field07->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 6)));
		ui->field08->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 7)));
		ui->field09->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 0)));
		ui->field10->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 1)));
		ui->field11->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 2)));
		ui->field12->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 3)));
		ui->field13->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 4)));
		ui->field14->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 5)));
		ui->field15->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 6)));
		ui->field16->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 7)));
		ui->field17->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 0)));
		ui->field18->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 1)));
		ui->field19->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 2)));
		ui->field20->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 3)));
		ui->field21->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 4)));
		ui->field22->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 5)));
		ui->field23->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 6)));
		ui->field24->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 7)));
		ui->field25->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 0)));
		ui->field26->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 1)));
		ui->field27->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 2)));
		ui->field28->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 3)));
		ui->field29->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 4)));
		ui->field30->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 5)));
		ui->field31->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 6)));
		ui->field32->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 7)));
		ui->field33->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 0)));
		ui->field34->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 1)));
		ui->field35->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 2)));
		ui->field36->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 3)));
		ui->field37->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 4)));
		ui->field38->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 5)));
		ui->field39->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 6)));
		ui->field40->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 7)));
		ui->field41->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 0)));
		ui->field42->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 1)));
		ui->field43->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 2)));
		ui->field44->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 3)));
		ui->field45->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 4)));
		ui->field46->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 5)));
		ui->field47->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 6)));
		ui->field48->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 7)));
		ui->field49->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 0)));
		ui->field50->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 1)));
		ui->field51->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 2)));
		ui->field52->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 3)));
		ui->field53->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 4)));
		ui->field54->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 5)));
		ui->field55->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 6)));
		ui->field56->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 7)));
		ui->field57->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 0)));
		ui->field58->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 1)));
		ui->field59->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 2)));
		ui->field60->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 3)));
		ui->field61->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 4)));
		ui->field62->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 5)));
		ui->field63->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 6)));
		ui->field64->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 7)));
	}
}

//
void
MainWindow::printSea (Sea& xSea)
{
	// Nesto bitno

	{

		ui->field01->setStyleSheet (fieldColor (xSea.getField (0, 0)));
		ui->field02->setStyleSheet (fieldColor (xSea.getField (0, 1)));
		ui->field03->setStyleSheet (fieldColor (xSea.getField (0, 2)));
		ui->field04->setStyleSheet (fieldColor (xSea.getField (0, 3)));
		ui->field05->setStyleSheet (fieldColor (xSea.getField (0, 4)));
		ui->field06->setStyleSheet (fieldColor (xSea.getField (0, 5)));
		ui->field07->setStyleSheet (fieldColor (xSea.getField (0, 6)));
		ui->field08->setStyleSheet (fieldColor (xSea.getField (0, 7)));
		ui->field09->setStyleSheet (fieldColor (xSea.getField (1, 0)));
		ui->field10->setStyleSheet (fieldColor (xSea.getField (1, 1)));
		ui->field11->setStyleSheet (fieldColor (xSea.getField (1, 2)));
		ui->field12->setStyleSheet (fieldColor (xSea.getField (1, 3)));
		ui->field13->setStyleSheet (fieldColor (xSea.getField (1, 4)));
		ui->field14->setStyleSheet (fieldColor (xSea.getField (1, 5)));
		ui->field15->setStyleSheet (fieldColor (xSea.getField (1, 6)));
		ui->field16->setStyleSheet (fieldColor (xSea.getField (1, 7)));
		ui->field17->setStyleSheet (fieldColor (xSea.getField (2, 0)));
		ui->field18->setStyleSheet (fieldColor (xSea.getField (2, 1)));
		ui->field19->setStyleSheet (fieldColor (xSea.getField (2, 2)));
		ui->field20->setStyleSheet (fieldColor (xSea.getField (2, 3)));
		ui->field21->setStyleSheet (fieldColor (xSea.getField (2, 4)));
		ui->field22->setStyleSheet (fieldColor (xSea.getField (2, 5)));
		ui->field23->setStyleSheet (fieldColor (xSea.getField (2, 6)));
		ui->field24->setStyleSheet (fieldColor (xSea.getField (2, 7)));
		ui->field25->setStyleSheet (fieldColor (xSea.getField (3, 0)));
		ui->field26->setStyleSheet (fieldColor (xSea.getField (3, 1)));
		ui->field27->setStyleSheet (fieldColor (xSea.getField (3, 2)));
		ui->field28->setStyleSheet (fieldColor (xSea.getField (3, 3)));
		ui->field29->setStyleSheet (fieldColor (xSea.getField (3, 4)));
		ui->field30->setStyleSheet (fieldColor (xSea.getField (3, 5)));
		ui->field31->setStyleSheet (fieldColor (xSea.getField (3, 6)));
		ui->field32->setStyleSheet (fieldColor (xSea.getField (3, 7)));
		ui->field33->setStyleSheet (fieldColor (xSea.getField (4, 0)));
		ui->field34->setStyleSheet (fieldColor (xSea.getField (4, 1)));
		ui->field35->setStyleSheet (fieldColor (xSea.getField (4, 2)));
		ui->field36->setStyleSheet (fieldColor (xSea.getField (4, 3)));
		ui->field37->setStyleSheet (fieldColor (xSea.getField (4, 4)));
		ui->field38->setStyleSheet (fieldColor (xSea.getField (4, 5)));
		ui->field39->setStyleSheet (fieldColor (xSea.getField (4, 6)));
		ui->field40->setStyleSheet (fieldColor (xSea.getField (4, 7)));
		ui->field41->setStyleSheet (fieldColor (xSea.getField (5, 0)));
		ui->field42->setStyleSheet (fieldColor (xSea.getField (5, 1)));
		ui->field43->setStyleSheet (fieldColor (xSea.getField (5, 2)));
		ui->field44->setStyleSheet (fieldColor (xSea.getField (5, 3)));
		ui->field45->setStyleSheet (fieldColor (xSea.getField (5, 4)));
		ui->field46->setStyleSheet (fieldColor (xSea.getField (5, 5)));
		ui->field47->setStyleSheet (fieldColor (xSea.getField (5, 6)));
		ui->field48->setStyleSheet (fieldColor (xSea.getField (5, 7)));
		ui->field49->setStyleSheet (fieldColor (xSea.getField (6, 0)));
		ui->field50->setStyleSheet (fieldColor (xSea.getField (6, 1)));
		ui->field51->setStyleSheet (fieldColor (xSea.getField (6, 2)));
		ui->field52->setStyleSheet (fieldColor (xSea.getField (6, 3)));
		ui->field53->setStyleSheet (fieldColor (xSea.getField (6, 4)));
		ui->field54->setStyleSheet (fieldColor (xSea.getField (6, 5)));
		ui->field55->setStyleSheet (fieldColor (xSea.getField (6, 6)));
		ui->field56->setStyleSheet (fieldColor (xSea.getField (6, 7)));
		ui->field57->setStyleSheet (fieldColor (xSea.getField (7, 0)));
		ui->field58->setStyleSheet (fieldColor (xSea.getField (7, 1)));
		ui->field59->setStyleSheet (fieldColor (xSea.getField (7, 2)));
		ui->field60->setStyleSheet (fieldColor (xSea.getField (7, 3)));
		ui->field61->setStyleSheet (fieldColor (xSea.getField (7, 4)));
		ui->field62->setStyleSheet (fieldColor (xSea.getField (7, 5)));
		ui->field63->setStyleSheet (fieldColor (xSea.getField (7, 6)));
		ui->field64->setStyleSheet (fieldColor (xSea.getField (7, 7)));
	}
}

MainWindow::~MainWindow () { delete ui; }


QString colorDefault = "background-color: rgb(0, 85, 255)";

// Poslace signal u formatu "xy"
void
MainWindow::hitField (int x, int y)
{

	QString xy = QString::number (x);
	xy.append (QString::number (y));
	socket->write (QString (xy).toUtf8 ());
}

// Metod za citanje mora iz stringa (Poruka od servera)
Sea
MainWindow::readSea (QString clientMessage, int ID)
{

	Sea xSea;
	// Indikator o cijem moru se radi ovde ne treba!

	QString c = clientMessage.at (0);
	int xId = c.toInt ();
	//   ui->textEdit->append( "\n READSEA" +QString::number(ID) +
	//   QString::number(xId) +"\n");
	int i = 0;
	int help1 = 0;
	int help2 = 0;
	if (xId != ID) {
		i = 1;

	} else {
		i = 66;
		help1 = 65;
		help2 = 1;
	}

    for (; i < 65 + help1; i++) {
		c = clientMessage.at (i);
		xId = c.toInt ();
		Ship xShip (xId, 1);
		xSea.setShip ((i - 1 - help1) / 8, (i - 1 - help2) % 8, xShip);
	}

	return xSea;
}

// Metod za citanje mora iz fajla layouts.dat
void
MainWindow::readSeaFromFile ()
{
	QFile file ("layouts.dat");
	QTextStream inStr (&file);
	if (!file.open (QIODevice::ReadOnly | QIODevice::Text)) {
		ui->textEdit->append ("Grska prilikom citanja fajla!");
	}
	// Povesti racuna o izuzetku ovde

	// QString stringSea;
	while (!inStr.atEnd ()) {

		// Obrisemo stari raspored
        deleteSea ();
		QString stringSea = inStr.readLine ().trimmed ();
		// testiranje
		qDebug () << stringSea << " : " << stringSea.size ();

		// Provera da stringSea mozda nije prazan
		if (stringSea.size () != 64)
			continue;

		for (int i = 0; i < 64; i++) {
			QString c = stringSea.at (i);
			int xId = c.toInt ();
			Ship xShip (xId, 1);
			seaHome.setShip (i / 8, i % 8, xShip);
		}
		fleetLayouts.push_back (seaHome);
		qDebug () << "Velicina vektora: " << fleetLayouts.size ();
	}

	file.close ();
}

// Metod za pretvaranje mora u string
QString
MainWindow::writeSea (Sea xSea)
{
	QString stringSea = "";
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			int xId = xSea.getField (i, j).getId ();
			stringSea.append (QString::number (xId));
		}
	}

	return stringSea;
}

// Dugme: Ispisi sadrzaj vektora --------------- Testiranje
void
MainWindow::writeSeaVector ()
{

	QVector<Sea>::iterator it = fleetLayouts.begin ();

    for (; it != fleetLayouts.end (); it++) {
		ui->textEdit->append (writeSea (*it) + "\n");
	}
}

// BP oznacava redni broj poruke
// BP = 0 za prvo pozivanje metoda readyRead() i tada se postavlja clientID
int BP = 0;

void
MainWindow::readyRead ()
{

    ui->hitButton->setEnabled (true);

	QString line = QString::fromUtf8 (socket->readLine ()).trimmed ();

	if (BP == 0) {
		// Pogledaj metod incomingconnection u bsserver.cpp
		QString c = line.at (0);
		clientID = c.toInt ();
		QString lblText = "Player_" + QString::number (clientID);

		ui->label_player->setText (lblText);
		// Ovo radi ispravno
	} else {

		// Glavna stvar koja se desava na klijentskoj strani:
		// Provera koji klijent je poslao poruku
		QString s = line.at (0);
		if (s.toInt () == clientID && line.length () > 65)
			s = line.at (65);
		int seaID = s.toInt ();

		if (seaID == clientID)
            ui->hitButton->setEnabled (false);

		// Citanje koordinata iz poruke
		QString strX = line.at (1);
		QString strY = line.at (2);
		int x = strX.toInt ();
		int y = strY.toInt ();

		// Ovde se ispisuje informacija o cijem moru se radi
		// (testiranje) ui->lineEdit->setText("seaID: " +
		// QString::number(seaID));
		//    ui->textEdit->append( "\n" +QString::number(seaID) +
		//    QString::number(clientID)+ QString::number(BP)  +
		//    QString::number(line.length())+   "\n");

		// Ako je poruka od servera duza od 3 karaktera
		// (bice 65) znaci da ta poruka sadrzi protivnicko more
		if (((line.length () > 3) && (clientID == 0) && BP == 2)
		    || ((line.length () > 3) && (clientID == 1) && BP == 1)) {
			// ui->textEdit->append( "\n Prvo");
			seaGuest = readSea (line, clientID);
		} else if (seaID == clientID) {
			//   ui->textEdit->append( "\n Drugo");
			seaGuest.setHit (x, y);
			printSea (seaGuest);
		} else if (line.length () < 4) {
			//   ui->textEdit->append( "\n Trece");
			seaHome.setHit (x, y);
			printSeaHost (seaHome);
		}
	}

	ui->textEdit->append (line);

	// Player_num labela obavestava klijenta koji je igrac
	// i oznacava da je to prva poruka koju je poslao server

	BP++; // Sledeci put nece da cita player_num
}

// normalno za sad je localhost nece uvek biti
void
MainWindow::onConnectClicked ()
{
	// Ako su postavljeni svi brodovi onda se konektuje

	if (seaHome.getShipsLeft (0) == 0 && seaHome.getShipsLeft (1) == 0
	    && seaHome.getShipsLeft (2) == 0 && seaHome.getShipsLeft (3) == 0)
		socket->connectToHost ("localhost", 4200);
	else
		ui->lineEdit->setText ("Niste ubacili sve brodove");
}

void
MainWindow::connected ()
{
	// Flip over to the chat page:
	QString stringSea = QString::number (clientID);
	QString rest = writeSea (seaHome);
	stringSea.append (rest);

	// And send our username to the chat server.
	socket->write (QString (stringSea).toUtf8 ());
	// socket->write(QString("/me:" + ui->lineEdit->text() +
	// "\n").toUtf8());
}

// Nova flota
void MainWindow::on_newFleet_clicked()
{
    std::random_shuffle (fleetLayouts.begin (), fleetLayouts.end ());

    deleteSea ();
    if (!fleetLayouts.isEmpty ()) {
        seaHome = fleetLayouts.back ();
        printSeaHostText (seaHome);
        printSeaHost (seaHome);

        // Ovo se takodje desava i u createShip
        // ali ovde se to radi malo drugacije
        seaHome.setShipsLeft (0, 0);
        seaHome.setShipsLeft (1, 0);
        seaHome.setShipsLeft (2, 0);
        seaHome.setShipsLeft (3, 0);
    } else
        ui->textEdit->setText ("Greska prilikom citanja fajla!");
}

void MainWindow::on_deleteSea_clicked()
{
    deleteSea ();
    printSeaHostText (seaHome);
    printSeaHost (seaHome);
}

void MainWindow::on_insertShipPart_clicked()
{
    int x1, x2, y1, y2;
    // x1 = ui->textEdit_2->text
    x1 = ui->lineEdit_x1->text ().toInt ();
    x2 = ui->lineEdit_x2->text ().toInt ();
    y1 = ui->lineEdit_y1->text ().toInt ();
    y2 = ui->lineEdit_y2->text ().toInt ();

    if (seaHome.createShip (x1, y1, x2, y2, id)) {
        printSeaHostText (seaHome);
        id++;
        if (!ui->checkBox->isChecked ())
            printSeaHost (seaHome);

    } else
        ui->textEdit->setText ("Brod nije uspesno kreiran");

    ui->lineEdit_x1->clearFocus ();
}

void MainWindow::on_horizontalFlip_clicked()
{
    randomSea::horizontalFlip (seaHome);
    printSeaHostText (seaHome);
    printSeaHost (seaHome);
}

void MainWindow::on_verticalFlip_clicked()
{
    randomSea::verticalFlip (seaHome);
    printSeaHostText (seaHome);
    printSeaHost (seaHome);
}

void MainWindow::on_hitButton_clicked()
{
    int x = ui->lineEditFire_x1->text ().toInt ();
    int y = ui->lineEditFire_y1->text ().toInt ();

    // Salje koordinate serveru
    hitField (x, y);
}

// Prikaz - domaci / gosti
void MainWindow::on_checkBox_clicked()
{
    if (ui->checkBox->checkState ()) {
        printSea (seaGuest);
        ui->checkBox->setText ("Gosti");
    } else {
        printSeaHost (seaHome);
        ui->checkBox->setText ("Domaci");
    }
}
