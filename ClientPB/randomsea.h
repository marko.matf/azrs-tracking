#ifndef RANDOMSEA_H
#define RANDOMSEA_H
#include "sea.h"

/* Klasa randomSea koristi transformacije da bi iz prethodno
    kreiranih flota napravila novi (nasumicni) raspored */
class randomSea
{
      public:
	randomSea ();
	static void symetricMainDiagonal (Sea& xSea);
	static void symetricSecondaryDiagonal (Sea& xSea);
	static void rotateDirect (Sea& xSea);
	static void horizontalFlip (Sea& xSea);
	static void verticalFlip (Sea& xSea);
};

#endif // RANDOMSEA_H
