#include "field.h"
#include "mainwindow.h"
#include "sea.h"
#include "ship.h"
#include <QApplication>
#include <QPushButton>
#include <QToolButton>
#include <QVBoxLayout>
#include <QtNetwork/QDnsLookup>
#include <QtNetwork/QNetworkAccessManager>
#include <iostream>
#include <vector>

int
main (int argc, char* argv[])
{
	QApplication a (argc, argv);
	MainWindow w;

	w.show ();

	return a.exec ();
}
