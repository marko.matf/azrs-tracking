#ifndef FIELD_H
#define FIELD_H

class Field
{
      public:
	Field ();
	~Field ();
	bool isHit ();
	int getId ();
	void setId (int x);
	void setHit ();

      private:
	bool hit;
	int id; // Polje ima id razlicit od 0 samo ako je to polje Brod
};

#endif // FIELD_H
