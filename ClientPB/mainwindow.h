#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QTcpSocket>

#include "sea.h"
#include <QMainWindow>

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

      public:
	explicit MainWindow (QWidget* parent = 0);
	~MainWindow ();
	void printSeaHost (Sea& xSea);
	void printSea (Sea& xSea);
	void printSeaHostText (Sea& xSea);
	void hitField (int x, int y);
    Sea createSea ();
    void deleteSea();
    void writeSeaVector();
	// writeSea je ispravan
	QString writeSea (Sea xSea);
	Sea readSea (QString clientMessage, int ID);
	void readSeaFromFile ();
	// Napraviti sendSea(), koji koristi funkciju writeSea

      private slots:

	// Kemar dugme
	void onConnectClicked ();

	// This gets called when you click the sayButton on
	// the chat page.

	void connected ();
	// This is a function we'll connect to a socket's readyRead()
	// signal, which tells us there's text to be read from the chat
	// server.
	void readyRead ();

    void on_checkBox_clicked ();

    void on_newFleet_clicked();

    void on_deleteSea_clicked();

    void on_insertShipPart_clicked();

    void on_horizontalFlip_clicked();

    void on_verticalFlip_clicked();

    void on_hitButton_clicked();

private:
	Ui::MainWindow* ui;
	QTcpSocket* socket;
	int clientID;
	Sea seaHome;
	Sea seaGuest;
	QVector<Sea> fleetLayouts;
};

#endif // MAINWINDOW_H
