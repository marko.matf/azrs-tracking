#-------------------------------------------------
#
# Project created by QtCreator 2015-06-06T17:38:17
#
#-------------------------------------------------

QT       += core gui
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ClientPB
TEMPLATE = app

DEPENDPATH += .
INCLUDEPATH += .



SOURCES += main.cpp\
        mainwindow.cpp \
    field.cpp \
    randomsea.cpp \
    ship.cpp \
    sea.cpp

HEADERS  += mainwindow.h \
    field.h \
    randomsea.h \
    ship.h \
    sea.h

FORMS    += mainwindow.ui
