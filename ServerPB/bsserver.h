#ifndef BSSERVER_H
#define BSSERVER_H

#include "sea.h"
#include <QMap>
#include <QSet>
#include <QStringList>
#include <QTcpServer>
#include <QTcpSocket>

class BSServer : public QTcpServer
{
	Q_OBJECT

      public:
	BSServer (QObject* parent = 0);
	Sea readSea (QString clientMessage);
	QString writeSea (Sea xSea);
	QString makeStringSea (Sea xSea, int clientID);
	QString makeStringXY (int x, int y, int clientID);
	void sendSeas ();

      private slots:
	void readyRead ();
	void disconnected ();

      protected:
	void incomingConnection (int socketID);

      private:
	int num;
	Sea seaPlayer1;
	Sea seaPlayer2;
	QSet<QTcpSocket*> clients;
	QSet<QTcpSocket*> players;
	QMap<QTcpSocket*, QString> users;
};

#endif
