#include "field.h"

Field::Field ()
{
	hit = false;
	id = 0;
}

Field::~Field () = default;

// getters & setters
auto
Field::isHit () -> bool
{
	return hit;
}

void
Field::setHit ()
{
	hit = true;
}

auto
Field::getId () -> int
{
	return id;
}

void
Field::setId (int x)
{
	id = x;
}
