#-------------------------------------------------
#
# Project created by QtCreator 2015-06-06T16:05:14
#
#-------------------------------------------------

QT       += core gui
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ServerPB
TEMPLATE = app
DEPENDPATH += .
INCLUDEPATH += .




SOURCES += main.cpp\
        mainwindow.cpp \
    field.cpp \
    randomsea.cpp \
    sea.cpp \
    ship.cpp \
    bsserver.cpp

HEADERS  += mainwindow.h \
    field.h \
    randomsea.h \
    sea.h \
    ship.h \
    bsserver.h

FORMS    += mainwindow.ui
