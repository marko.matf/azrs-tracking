#ifndef SHIP_H
#define SHIP_H
#include "field.h"

class Ship : public Field
{
      public:
	Ship (int xId, int xSize);
	~Ship ();
	int getSize ();
	void setSize (int x);

      private:
	int size;
};

#endif // SHIP_H
