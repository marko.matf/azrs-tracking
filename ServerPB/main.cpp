#include "field.h"
#include "mainwindow.h"
#include "sea.h"
#include "ship.h"
#include "ui_mainwindow.h"
#include <QApplication>
#include <QPushButton>
#include <QToolButton>
#include <QVBoxLayout>
#include <QtNetwork/QDnsLookup>
#include <QtNetwork/QNetworkAccessManager>
#include <iostream>
#include <vector>

#include "bsserver.h"

using namespace std;

int
main (int argc, char* argv[])
{
	QApplication a (argc, argv);
	MainWindow w;

	w.show ();

	BSServer* server = new BSServer ();
	bool success = server->listen (QHostAddress::Any, 4200);
	if (!success) {
		qFatal ("Could not listen on port 4200.");
	}

	qDebug () << "Ready";

	/*
	Sea Jadran;
	Jadran.printSea();
	int id = 1;

	Jadran.createShipRandom(3,id);
	id++;
	Jadran.createShipRandom(2,id);
	id++;
	Jadran.createShipRandom(1,id);
	id++;
	*/
	/*
	cout << "Original:" << endl;
	Sea Jadran = createSea();

	Jadran.printSeaHost();

	cout << endl;
	cout << "Simetrija po glavnoj dijagonali:" << endl;
	symetricMainDiagonal(Jadran);
	Jadran.printSeaHost();*/
	/*
	    cout << "Simetrija po sporednoj dijagonali:" << endl;
	    symetricSecondaryDiagonal(Jadran);
	    Jadran.printSeaHost();

	    */
	return a.exec ();
}
