#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "sea.h"
#include <QMainWindow>

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

      public:
	explicit MainWindow (QWidget* parent = 0);
	~MainWindow ();
	void printSeaHost (Sea& xSea);
	void printSea (Sea& xSea);
	void printSeaHostText (Sea& xSea);
	static Sea Moure;

      private slots:
	void on_pushButton_clicked ();

	void on_field01_clicked ();

	void on_pushButton_2_clicked ();

	void on_checkBox_clicked ();

	void on_pushButton_3_clicked ();

	void on_pushButton_4_clicked ();

	void on_pushButton_5_clicked ();

	void on_pushButton_6_clicked ();

	void on_pushButton_7_clicked ();

	void on_pushButton_8_clicked ();

	void on_pushButton_9_clicked ();

      private:
	Ui::MainWindow* ui;
};

#endif // MAINWINDOW_H
