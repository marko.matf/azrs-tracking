#ifndef SEA_H
#define SEA_H
#include "field.h"
#include "ship.h"
#include <vector>

class Sea
{
      public:
	Sea ();
	~Sea ();
	Field& getField (int i, int j); // Referenca se koristi jer getField
					// sluzi za menjanje konkretnog objekta
	void setShip (int i, int j, Ship xShip);
	bool createShip (int x1, int y1, int x2, int y2, int xId);
	void printSeaHost ();
	void printSea ();
	void createShipRandom (int xSize, int xId);
	bool fieldBusy (int x1, int y1, int x2, int y2);
	void setHit (int x, int y);
	bool isSunk (int x, int y);
	void sinkAround (int xId); // Funkcija postavlja sva polja oko broda na
				   // setHit (pogodjeno)

      private:
	Field sea[8][8];
};

#endif // SEA_H
