#include "mainwindow.h"
#include "bsserver.h"
#include "randomsea.h"
#include "sea.h"
#include "ui_mainwindow.h"
#include <QString>
int i = 1;
int id = 1;
Sea Jadran;

// BSServer *server = new BSServer();
// bool success = server->listen(QHostAddress::Any, 4200);

// QToolButton* makeButtons();

MainWindow::MainWindow (QWidget* parent)
    : QMainWindow (parent), ui (new Ui::MainWindow)
{
	// Mainwindow setup ----> ovde se namesta sve pre pocetka igre
	ui->setupUi (this);
	// Unos koordinata 0 - 7
	ui->lineEdit_x1->setValidator (new QIntValidator (0, 7, this));
	ui->lineEdit_y1->setValidator (new QIntValidator (0, 7, this));
	ui->lineEdit_x2->setValidator (new QIntValidator (0, 7, this));
	ui->lineEdit_y2->setValidator (new QIntValidator (0, 7, this));
	ui->lineEditFire_x1->setValidator (new QIntValidator (0, 7, this));
	ui->lineEditFire_y1->setValidator (new QIntValidator (0, 7, this));
	printSeaHost (Jadran);

	ui->gridLayout->setEnabled (true);

	//    if(!success)
	//    {
	//        qFatal("Could not listen on port 4200.");
	//    }

	//    qDebug() << "Ready";
}

// Ograniciti da se ne unosi nista sem int
void
MainWindow::on_pushButton_clicked () // Text --- Unesi brod
{
	int x1, x2, y1, y2;
	// x1 = ui->textEdit_2->text
	x1 = ui->lineEdit_x1->text ().toInt ();
	x2 = ui->lineEdit_x2->text ().toInt ();
	y1 = ui->lineEdit_y1->text ().toInt ();
	y2 = ui->lineEdit_y2->text ().toInt ();

	if (Jadran.createShip (x1, y1, x2, y2, id)) {
		printSeaHostText (Jadran);
		id++;
		if (!ui->checkBox->isChecked ())
			printSeaHost (Jadran);

	} else
		ui->textEdit->setText ("Brod nije uspesno kreiran");

	ui->lineEdit_x1->clearFocus ();
}

QString
fieldColor (Field xField)
{

	if (xField.isHit ()) {
		if (xField.getId () != 0)
			return "background-color: rgb(255, 0, 0)";
		else
			return "background-color: rgb(40, 0, 120)";
	} else
		return "background-color: rgb(0, 85, 255)";
}

QString
fieldColorHost (Field xField)
{

	if (xField.isHit ()) {
		if (xField.getId () != 0)
			return "background-color: rgb(255, 0, 0)";
		else
			return "background-color: rgb(40, 0, 120)";
	} else {
		if (xField.getId () != 0)
			return "background-color: rgb(0, 255, 0)";
		else
			return "background-color: rgb(0, 85, 255)";
	}
}

void
MainWindow::printSeaHostText (Sea& xSea)
{
	ui->textEdit->setText ("");
	for (unsigned int k = 0; k < 8; k++) {
		for (unsigned int j = 0; j < 8; j++) {

			if (xSea.getField (k, j).isHit ()) {
				ui->textEdit->insertPlainText ("x  ");
			} else if (xSea.getField (k, j).getId ()
				   != 0) // Provera da li je dato Polje zauzeto
					 // Brodom
			{
				// SetConsoleTextAttribute(h, FOREGROUND_GREEN
				// |
				//      FOREGROUND_INTENSITY);

				ui->textEdit->insertPlainText ("q  ");
			} else {
				// SetConsoleTextAttribute(h, FOREGROUND_RED |
				//      FOREGROUND_INTENSITY);
				ui->textEdit->insertPlainText ("o  ");
			}
		}
		// cout << endl;
		ui->textEdit->append ("");
	}
}

void
MainWindow::printSeaHost (Sea& xSea)
{

	// Nesto bitno

	{

		ui->field01->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 0)));
		ui->field02->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 1)));
		ui->field03->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 2)));
		ui->field04->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 3)));
		ui->field05->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 4)));
		ui->field06->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 5)));
		ui->field07->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 6)));
		ui->field08->setStyleSheet (
		    fieldColorHost (xSea.getField (0, 7)));
		ui->field09->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 0)));
		ui->field10->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 1)));
		ui->field11->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 2)));
		ui->field12->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 3)));
		ui->field13->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 4)));
		ui->field14->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 5)));
		ui->field15->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 6)));
		ui->field16->setStyleSheet (
		    fieldColorHost (xSea.getField (1, 7)));
		ui->field17->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 0)));
		ui->field18->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 1)));
		ui->field19->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 2)));
		ui->field20->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 3)));
		ui->field21->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 4)));
		ui->field22->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 5)));
		ui->field23->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 6)));
		ui->field24->setStyleSheet (
		    fieldColorHost (xSea.getField (2, 7)));
		ui->field25->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 0)));
		ui->field26->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 1)));
		ui->field27->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 2)));
		ui->field28->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 3)));
		ui->field29->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 4)));
		ui->field30->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 5)));
		ui->field31->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 6)));
		ui->field32->setStyleSheet (
		    fieldColorHost (xSea.getField (3, 7)));
		ui->field33->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 0)));
		ui->field34->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 1)));
		ui->field35->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 2)));
		ui->field36->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 3)));
		ui->field37->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 4)));
		ui->field38->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 5)));
		ui->field39->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 6)));
		ui->field40->setStyleSheet (
		    fieldColorHost (xSea.getField (4, 7)));
		ui->field41->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 0)));
		ui->field42->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 1)));
		ui->field43->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 2)));
		ui->field44->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 3)));
		ui->field45->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 4)));
		ui->field46->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 5)));
		ui->field47->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 6)));
		ui->field48->setStyleSheet (
		    fieldColorHost (xSea.getField (5, 7)));
		ui->field49->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 0)));
		ui->field50->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 1)));
		ui->field51->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 2)));
		ui->field52->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 3)));
		ui->field53->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 4)));
		ui->field54->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 5)));
		ui->field55->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 6)));
		ui->field56->setStyleSheet (
		    fieldColorHost (xSea.getField (6, 7)));
		ui->field57->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 0)));
		ui->field58->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 1)));
		ui->field59->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 2)));
		ui->field60->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 3)));
		ui->field61->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 4)));
		ui->field62->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 5)));
		ui->field63->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 6)));
		ui->field64->setStyleSheet (
		    fieldColorHost (xSea.getField (7, 7)));
	}
}

void
MainWindow::printSea (Sea& xSea)
{
	// Nesto bitno

	{

		ui->field01->setStyleSheet (fieldColor (xSea.getField (0, 0)));
		ui->field02->setStyleSheet (fieldColor (xSea.getField (0, 1)));
		ui->field03->setStyleSheet (fieldColor (xSea.getField (0, 2)));
		ui->field04->setStyleSheet (fieldColor (xSea.getField (0, 3)));
		ui->field05->setStyleSheet (fieldColor (xSea.getField (0, 4)));
		ui->field06->setStyleSheet (fieldColor (xSea.getField (0, 5)));
		ui->field07->setStyleSheet (fieldColor (xSea.getField (0, 6)));
		ui->field08->setStyleSheet (fieldColor (xSea.getField (0, 7)));
		ui->field09->setStyleSheet (fieldColor (xSea.getField (1, 0)));
		ui->field10->setStyleSheet (fieldColor (xSea.getField (1, 1)));
		ui->field11->setStyleSheet (fieldColor (xSea.getField (1, 2)));
		ui->field12->setStyleSheet (fieldColor (xSea.getField (1, 3)));
		ui->field13->setStyleSheet (fieldColor (xSea.getField (1, 4)));
		ui->field14->setStyleSheet (fieldColor (xSea.getField (1, 5)));
		ui->field15->setStyleSheet (fieldColor (xSea.getField (1, 6)));
		ui->field16->setStyleSheet (fieldColor (xSea.getField (1, 7)));
		ui->field17->setStyleSheet (fieldColor (xSea.getField (2, 0)));
		ui->field18->setStyleSheet (fieldColor (xSea.getField (2, 1)));
		ui->field19->setStyleSheet (fieldColor (xSea.getField (2, 2)));
		ui->field20->setStyleSheet (fieldColor (xSea.getField (2, 3)));
		ui->field21->setStyleSheet (fieldColor (xSea.getField (2, 4)));
		ui->field22->setStyleSheet (fieldColor (xSea.getField (2, 5)));
		ui->field23->setStyleSheet (fieldColor (xSea.getField (2, 6)));
		ui->field24->setStyleSheet (fieldColor (xSea.getField (2, 7)));
		ui->field25->setStyleSheet (fieldColor (xSea.getField (3, 0)));
		ui->field26->setStyleSheet (fieldColor (xSea.getField (3, 1)));
		ui->field27->setStyleSheet (fieldColor (xSea.getField (3, 2)));
		ui->field28->setStyleSheet (fieldColor (xSea.getField (3, 3)));
		ui->field29->setStyleSheet (fieldColor (xSea.getField (3, 4)));
		ui->field30->setStyleSheet (fieldColor (xSea.getField (3, 5)));
		ui->field31->setStyleSheet (fieldColor (xSea.getField (3, 6)));
		ui->field32->setStyleSheet (fieldColor (xSea.getField (3, 7)));
		ui->field33->setStyleSheet (fieldColor (xSea.getField (4, 0)));
		ui->field34->setStyleSheet (fieldColor (xSea.getField (4, 1)));
		ui->field35->setStyleSheet (fieldColor (xSea.getField (4, 2)));
		ui->field36->setStyleSheet (fieldColor (xSea.getField (4, 3)));
		ui->field37->setStyleSheet (fieldColor (xSea.getField (4, 4)));
		ui->field38->setStyleSheet (fieldColor (xSea.getField (4, 5)));
		ui->field39->setStyleSheet (fieldColor (xSea.getField (4, 6)));
		ui->field40->setStyleSheet (fieldColor (xSea.getField (4, 7)));
		ui->field41->setStyleSheet (fieldColor (xSea.getField (5, 0)));
		ui->field42->setStyleSheet (fieldColor (xSea.getField (5, 1)));
		ui->field43->setStyleSheet (fieldColor (xSea.getField (5, 2)));
		ui->field44->setStyleSheet (fieldColor (xSea.getField (5, 3)));
		ui->field45->setStyleSheet (fieldColor (xSea.getField (5, 4)));
		ui->field46->setStyleSheet (fieldColor (xSea.getField (5, 5)));
		ui->field47->setStyleSheet (fieldColor (xSea.getField (5, 6)));
		ui->field48->setStyleSheet (fieldColor (xSea.getField (5, 7)));
		ui->field49->setStyleSheet (fieldColor (xSea.getField (6, 0)));
		ui->field50->setStyleSheet (fieldColor (xSea.getField (6, 1)));
		ui->field51->setStyleSheet (fieldColor (xSea.getField (6, 2)));
		ui->field52->setStyleSheet (fieldColor (xSea.getField (6, 3)));
		ui->field53->setStyleSheet (fieldColor (xSea.getField (6, 4)));
		ui->field54->setStyleSheet (fieldColor (xSea.getField (6, 5)));
		ui->field55->setStyleSheet (fieldColor (xSea.getField (6, 6)));
		ui->field56->setStyleSheet (fieldColor (xSea.getField (6, 7)));
		ui->field57->setStyleSheet (fieldColor (xSea.getField (7, 0)));
		ui->field58->setStyleSheet (fieldColor (xSea.getField (7, 1)));
		ui->field59->setStyleSheet (fieldColor (xSea.getField (7, 2)));
		ui->field60->setStyleSheet (fieldColor (xSea.getField (7, 3)));
		ui->field61->setStyleSheet (fieldColor (xSea.getField (7, 4)));
		ui->field62->setStyleSheet (fieldColor (xSea.getField (7, 5)));
		ui->field63->setStyleSheet (fieldColor (xSea.getField (7, 6)));
		ui->field64->setStyleSheet (fieldColor (xSea.getField (7, 7)));
	}
}

MainWindow::~MainWindow () { delete ui; }

/*
 * MainWindow::MainWindow ---- ovde ubacuj ono sto
 * treba da se pojavi kad se pokrene igrica.
 *
 * Ovaj projekat sinhronizujemo sad
 *
 * Radio butons za biranje brodova
 *
 */
QString colorDefault = "background-color: rgb(0, 85, 255)";

void
MainWindow::on_field01_clicked ()
{
	// Jos treba da se ubaci select i deselect, za sad menja boje samo

	//    if(colorDefault.compare(ui->field01->styleSheet()) == 0)
	//        ui->field01->setStyleSheet("background-color: rgb(40, 0,
	//        120)");
	//    else
	//        ui->field01->setStyleSheet(colorDefault);
}

// Pucanje
void
MainWindow::on_pushButton_2_clicked ()
{
	int x1, y1;
	x1 = ui->lineEditFire_x1->text ().toInt ();
	y1 = ui->lineEditFire_y1->text ().toInt ();

	//    server->pucaj(x1,y1);

	Jadran.setHit (x1, y1);
	if (ui->checkBox->checkState ()) {
		printSea (Jadran);
		ui->checkBox->setText ("Gosti");
	} else {
		printSeaHost (Jadran);
		ui->checkBox->setText ("Domaci");
	}
}

// Prikaz: Domaci ili gosti
void
MainWindow::on_checkBox_clicked ()
{
	if (ui->checkBox->checkState ()) {
		printSea (Jadran);
		ui->checkBox->setText ("Gosti");
	} else {
		printSeaHost (Jadran);
		ui->checkBox->setText ("Domaci");
	}
}

// Kod do kraja sluzi samo za testiranje
Sea xSea;

// Dugme: Nova flota
void
MainWindow::on_pushButton_3_clicked ()
{
	int id = 1;
	xSea.createShip (5, 1, 5, 4, id); // 4
	id++;
	xSea.createShip (0, 3, 2, 3, id); // 3
	id++;
	xSea.createShip (1, 7, 3, 7, id); // 3
	id++;
	xSea.createShip (0, 1, 1, 1, id); // 2
	id++;
	xSea.createShip (3, 0, 3, 1, id); // 2
	id++;
	xSea.createShip (6, 6, 6, 7, id); // 2
	id++;
	xSea.createShip (1, 5, 1, 5, id); // 1
	id++;
	xSea.createShip (3, 5, 3, 5, id); // 1
	id++;
	xSea.createShip (7, 1, 7, 1, id); // 1
	id++;
	xSea.createShip (7, 3, 7, 3, id); // 1

	printSeaHostText (xSea);
	printSeaHost (xSea);
}

// Dugme: Glavna D
void
MainWindow::on_pushButton_4_clicked ()
{
	randomSea::symetricMainDiagonal (xSea);
	printSeaHostText (xSea);
	printSeaHost (xSea);
}

// Dugme: Sporedna D
void
MainWindow::on_pushButton_5_clicked ()
{
	randomSea::symetricSecondaryDiagonal (xSea);
	printSeaHostText (xSea);
	printSeaHost (xSea);
}

// Dugme: Rotiraj
void
MainWindow::on_pushButton_6_clicked ()
{
	randomSea::rotateDirect (xSea);
	printSeaHostText (xSea);
	printSeaHost (xSea);
}

// Dugme: Horizontala
void
MainWindow::on_pushButton_7_clicked ()
{
	randomSea::horizontalMiddle (xSea);
	printSeaHostText (xSea);
	printSeaHost (xSea);
}

// Dugme: Vertikala
void
MainWindow::on_pushButton_8_clicked ()
{
	randomSea::verticalMiddle (xSea);
	printSeaHostText (xSea);
	printSeaHost (xSea);
}

// Dugme: Del
void
MainWindow::on_pushButton_9_clicked ()
{
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			xSea.getField (i, j).setId (0);

	printSeaHostText (xSea);
	printSeaHost (xSea);
}
