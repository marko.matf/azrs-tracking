#include "randomsea.h"
#include "sea.h"
#include "ship.h"

randomSea::randomSea () = default;

// Ukupno 7 slika

void
randomSea::symetricMainDiagonal (Sea& xSea)
{

	Ship xShip (0, 1); // Ovaj brod se menja i postavlja
	int tmpId1 = 0;
	int tmpId2 = 0;

	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			// Da se ne bi obradjivalo svako polje, vec samo ona
			// polja zauzeta brodom
			if ((xSea.getField (i, j).getId () != 0)
			    || (xSea.getField (j, i).getId () != 0))
				if (i > j) {
					tmpId1 = xSea.getField (i, j).getId ();
					tmpId2 = xSea.getField (j, i).getId ();
					xShip.setId (tmpId1);
					xSea.setShip (j, i, xShip);
					xShip.setId (tmpId2);
					xSea.setShip (i, j, xShip);
				}
}

void
randomSea::symetricSecondaryDiagonal (Sea& xSea)
{
	Ship xShip (0, 1); // Ovaj brod se menja i postavlja
	int tmpId1 = 0;
	int tmpId2 = 0;
	int k = 8;

	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < k; j++) {
			// Da se ne bi obradjivalo svako polje, vec samo ona
			// polja zauzeta brodom
			if ((xSea.getField (i, j).getId () != 0)
			    || (xSea.getField (7 - j, 7 - i).getId () != 0))
				if ((i + j) != 7) {
					tmpId1 = xSea.getField (i, j).getId ();
					tmpId2 = xSea.getField (7 - j, 7 - i)
						     .getId ();
					xShip.setId (tmpId1);
					xSea.setShip (7 - j, 7 - i, xShip);
					xShip.setId (tmpId2);
					xSea.setShip (i, j, xShip);
				}
		}
		k--;
	}
}

// Rotiranje flote suprotono od kazaljke na satu
// Rotacija <=> glavna dijagonala + vertikala
// Rotacija <=> sporedna dijagonala + horizontala
void
randomSea::rotateDirect (Sea& xSea)
{
	randomSea::symetricMainDiagonal (xSea);
	randomSea::verticalMiddle (xSea);
	/*
	Sea newSea;
	int newId = 0;
	Ship newShip(0, 1);

	int k = 0;
	for(int i = 0; i < 8; i++)
	{
	    k = 7;
	    for(int j = 0; j < 8; j++)
	    {
		newId = xSea.getField(i, j).getId();
		// Brisemo stari raspored sa xSea
		xSea.getField(i, j).setId(0);
		newShip.setId(newId);
		newSea.setShip(k, i, newShip);
		k--;
	    }
	}

	// Kopiranje novog rasporeda u xSea
	for(int i = 0; i < 8; i++)
	{
	    for(int j = 0; j < 8; j++)
	    {
		newId = newSea.getField(i, j).getId();
		newShip.setId(newId);
		xSea.setShip(i, j, newShip);
	    }
	}
	*/
}
// Rotacija u smeru kazaljke na satu
// samo obrnut redosled --- vertikala + glavna dijagonala

//
void
randomSea::horizontalMiddle (Sea& xSea)
{

	Sea newSea;
	int newId = 0;
	Ship newShip (0, 1);

	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			newId = xSea.getField (i, j).getId ();
			// Brisemo stari raspored sa xSea
			xSea.getField (i, j).setId (0);
			newShip.setId (newId);
			newSea.setShip (i, 7 - j, newShip);
		}
	}

	// Kopiranje novog rasporeda u xSea
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			newId = newSea.getField (i, j).getId ();
			newShip.setId (newId);
			xSea.setShip (i, j, newShip);
		}
	}
}

//
void
randomSea::verticalMiddle (Sea& xSea)
{

	Sea newSea;
	int newId = 0;
	Ship newShip (0, 1);

	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			newId = xSea.getField (i, j).getId ();
			// Brisemo stari raspored sa xSea
			xSea.getField (i, j).setId (0);
			newShip.setId (newId);
			newSea.setShip (7 - i, j, newShip);
		}
	}

	// Kopiranje novog rasporeda u xSea
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			newId = newSea.getField (i, j).getId ();
			newShip.setId (newId);
			xSea.setShip (i, j, newShip);
		}
	}
}

/*
 *  v + Gd <===> H + Sd <===> rotate * 3 <===> rotate (u smeru kazaljke)
 *
 *
 *
 *
 *
 *
 *
 *
 */
