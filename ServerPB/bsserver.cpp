#include "bsserver.h"
#include "mainwindow.h"
#include "sea.h"
#include "ship.h"
#include "ui_mainwindow.h"

#include <iostream>

#include <QRegExp>
#include <QTcpSocket>

// Kad se konektuje klijent odmah dobija odgovor koji je
// po redu (0 ili 1). Na osnovu toga kasnije prepoznaje
// koje more je dobio i koje more treba da odstampa (svoje ili protivnicko)

BSServer::BSServer (QObject* parent) : QTcpServer (parent) { num = 0; }

// Metod za citanje mora iz stringa
Sea
BSServer::readSea (QString clientMessage)
{
	Sea xSea;
	// Indikator o cijem moru se radi ovde ne treba!
	for (int i = 1; i < 65; i++) {
		QString c = clientMessage.at (i);
		int xId = c.toInt ();
		Ship xShip (xId, 1);
		xSea.setShip ((i - 1) / 8, (i - 1) % 8, xShip);
	}
	return xSea;
}

// Metod za pretvaranje mora u string
QString
BSServer::writeSea (Sea xSea)
{
	QString stringSea = "";
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			int xId = xSea.getField (i, j).getId ();
			stringSea.append (QString::number (xId));
		}
	}

	return stringSea;
}

// socketDescriptor je int koji odredjuje socket i
// definise se u metodu incomingConnection
// socketDescriptor ostaje isti sve dok je klijent
// konektovan na server

// Niz koji cuva socketDescriptor od dva socketa
int socketIDs[2];
void
BSServer::incomingConnection (int socketID)
{
	if (num < 2) {
		socketIDs[num] = socketID;
		// Num se povecava kada se konektuje igrac, maksimum 2 igraca

		QTcpSocket* client = new QTcpSocket (this);
		client->setSocketDescriptor (socketID);
		// Ubacivanje u niz socketa
		// players.insert(client);

		clients.insert (client);
		QString playerNum = QString::number (num);
		// Ovde se klijentu odmah salje koji po redu se konektovao
		client->write (QString (playerNum).toUtf8 ());

		qDebug () << "New client from:"
			  << client->peerAddress ().toString ();

		connect (client, SIGNAL (readyRead ()), this,
			 SLOT (readyRead ()));
		connect (client, SIGNAL (disconnected ()), this,
			 SLOT (disconnected ()));

		num++;
	}
}

// int firstClientID = socketIDs[0];
// int secondClientID = socketIDs[1];

// QTcpSocket player1;
// QTcpSocket player2;

// Metod pakuje more u string, poruku koju server salje
QString
BSServer::makeStringSea (Sea xSea, int clientID)
{
	// clientID je prvi u nizu karaktera
	QString message = QString::number (clientID);
	QString rest = writeSea (xSea);
	message.append (rest);
	return message;
}

// Metod pakuje koordinate u string, poruku koju server salje
QString
BSServer::makeStringXY (int x, int y, int clientID)
{
	// clientID je prvi u nizu karaktera
	QString message = QString::number (clientID);
	message.append (QString::number (x));
	message.append (QString::number (y));
	return message;
}

// Metod pravi string duzine 130 karaktera, koji ce klijenti
// da interpretiraju kao string od 65 karaktera (pogledaj readSea())
void
BSServer::sendSeas ()
{

	QString stringSea;

	stringSea = BSServer::makeStringSea (seaPlayer1, 0);
	QString rest = BSServer::makeStringSea (seaPlayer2, 1);
	stringSea.append (rest);

	foreach (QTcpSocket* client, clients)
		client->write (QString (stringSea).toUtf8 ());
}

int k = 0;
// Fali provera kad je partija zavrsena!
void
BSServer::readyRead ()
{
	// Klijent koji emituje signal - sender()

	QTcpSocket* client = (QTcpSocket*)sender ();

	QString xy = QString::fromUtf8 (client->readLine ()).trimmed ();

	// Ako poruka ima vise od 4 karaktera klijent salje more, a
	// ako ima 3 karaktera onda salje koordinate
	if (xy.length () < 4) {
		QString strX = xy.at (0);
		QString strY = xy.at (1);

		int x = strX.toInt ();
		int y = strY.toInt ();

		if (client->socketDescriptor () == socketIDs[0]) {
			// Ako je onaj klijent koji se prvi konektovao sada
			// onaj koji je poruku poslao, tada se salje 1.....

			seaPlayer2.setHit (x, y);
			QString stringXY = BSServer::makeStringXY (x, y, 0);
			foreach (QTcpSocket* client, clients)
				client->write (QString (stringXY).toUtf8 ());

		} else {
			// Ako je ipak poslao poruku drugi po redu konektovani
			// igrac tada se salje 0....

			seaPlayer2.setHit (x, y);
			QString stringXY = BSServer::makeStringXY (x, y, 1);
			foreach (QTcpSocket* client, clients)
				client->write (QString (stringXY).toUtf8 ());
		}

	} else {
		if (k == 0) {
			seaPlayer1 = readSea (xy);
			qDebug () << xy;
			seaPlayer1.printSeaHost ();

			// U ovom trenutku na serveru se nalazi samo jedno more
			// Server treba da posalje oba mora
			// BSServer::sendSeas();
		} else // k = 1 ====> Player_1 je sender()
		{
			seaPlayer2 = readSea (xy);
			qDebug () << xy;
			seaPlayer2.printSeaHost ();

			// U ovom trenutku na serveru se nalaze oba mora
			// Server treba da posalje oba mora
		}
		BSServer::sendSeas ();
	}

	k++;
}

void
BSServer::disconnected ()
{
	num--;
	QTcpSocket* client = (QTcpSocket*)sender ();
	QString s = client->peerAddress ().toString ();
	int i = s.toInt ();
	qDebug () << "Client disconnected:";

	clients.remove (client);

	QString user = users[client];
	users.remove (client);

	foreach (QTcpSocket* client, clients)
		client->write (
		    QString ("Server:" + user + " has left.\n").toUtf8 ());
}
