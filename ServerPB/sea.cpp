#include "sea.h"
#include "mainwindow.h"
#include "ship.h"
#include "ui_mainwindow.h"
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>

using namespace std;

Sea::Sea () = default;

Sea::~Sea () = default;

auto
Sea::getField (int i, int j) -> Field&
{
	return sea[i][j];
}

void
Sea::setShip (int i, int j, Ship xShip)
{
	sea[i][j] = xShip;
}

// Nakon svakog gadjanja ispituje se da li je doslo do potopa
void
Sea::setHit (int x, int y)
{
	this->getField (x, y).setHit ();

	if (this->getField (x, y).getId () != 0) {
		if (this->isSunk (x, y))
			this->sinkAround (this->getField (x, y).getId ());
	}
}

// Nakon svakog gadjanja provera da li je doslo do potopa
auto
Sea::isSunk (int x, int y) -> bool
{
	int xId = this->getField (x, y).getId ();

	for (int i = 0; i < 8; i++) {
		if (this->getField (x, i).getId () == xId)
			if (!this->getField (x, i).isHit ())
				return false;

		if (this->getField (i, y).getId () == xId)
			if (!this->getField (i, y).isHit ())
				return false;
	}

	return true;
}

// Obelezavanje polja oko potopljenog broda
void
Sea::sinkAround (int xId)
{
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)

			if (this->getField (i, j).getId () == xId) {
				if (i - 1 >= 0 && j - 1 >= 0)
					this->getField (i - 1, j - 1)
					    .setHit ();
				if (i - 1 >= 0)
					this->getField (i - 1, j).setHit ();
				if (i - 1 >= 0 && j + 1 < 8)
					this->getField (i - 1, j + 1)
					    .setHit ();

				if (j - 1 >= 0)
					this->getField (i, j - 1).setHit ();
				if (j + 1 < 8)
					this->getField (i, j + 1).setHit ();

				if (i + 1 < 8 && j - 1 >= 0)
					this->getField (i + 1, j - 1)
					    .setHit ();
				if (i + 1 < 8)
					this->getField (i + 1, j).setHit ();
				if (i + 1 < 8 && j + 1 < 8)
					this->getField (i + 1, j + 1)
					    .setHit ();
			}
}

void
Sea::printSea ()
{
	// HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);

	// Kad se ubace koordinate 7, 7 ostane neka boja aktivna

	for (unsigned int i = 0; i < 8; i++) {
		for (unsigned int j = 0; j < 8; j++) {

			if (this->getField (i, j)
				.isHit ()) // Provera da li je dato Polje
					   // pogodjeno
				if (this->getField (i, j).getId ()
				    != 0) // Provera da li je dato Polje
					  // zauzeto Brodom
				{
					// SetConsoleTextAttribute(h,
					// FOREGROUND_GREEN |
					// FOREGROUND_INTENSITY);
					// MainWindow::ui->textEdit_3->append("*
					// ");
					cout << "* ";

				} else {
					// SetConsoleTextAttribute(h,
					// FOREGROUND_RED |
					//    FOREGROUND_INTENSITY);

					// MainWindow::ui->textEdit_3->append("x
					// ");
					cout << "x ";
				}
			else {
				// SetConsoleTextAttribute(h, FOREGROUND_BLUE |
				// FOREGROUND_GREEN | FOREGROUND_RED |
				//   FOREGROUND_INTENSITY);
				// MainWindow::ui->textEdit_3->append("x ");

				cout << "o ";
			}
		}
		cout << endl;
	}
	// Ukoliko se unesu koordinate 7, 7 poslednja promena boje moze biti
	// RED ili GREEN Resenje: na kraju uvek vracanje na staro ====>
	// SetConsoleTextAttribute(h, FOREGROUND_BLUE |
	//            FOREGROUND_GREEN | FOREGROUND_RED |
	//            FOREGROUND_INTENSITY);
}

void
Sea::printSeaHost ()
{
	// HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);

	for (unsigned int i = 0; i < 8; i++) {
		for (unsigned int j = 0; j < 8; j++) {
			if (this->getField (i, j).isHit ()) {
				cout << "x ";
			} else if (this->getField (i, j).getId ()
				   != 0) // Provera da li je dato Polje zauzeto
					 // Brodom
			{
				// SetConsoleTextAttribute(h, FOREGROUND_GREEN
				// |
				//      FOREGROUND_INTENSITY);
				cout << "x ";
			} else {
				// SetConsoleTextAttribute(h, FOREGROUND_RED |
				//      FOREGROUND_INTENSITY);
				cout << "- ";
			}
		}

		cout << endl;
	}
	// Ukoliko se unesu koordinate 7, 7 poslednja promena boje moze biti
	// RED ili GREEN Resenje: na kraju uvek vracanje na staro ====>
	// SetConsoleTextAttribute(h, FOREGROUND_BLUE |
	//            FOREGROUND_GREEN | FOREGROUND_RED |
	//            FOREGROUND_INTENSITY);
}

auto
Sea::createShip (int x1, int y1, int x2, int y2, int xId) -> bool
{
	/* Kreiranje broda s datim koordinatama pocetka i kraja */

	// ako x1 > x2 ====> for petlja ima drugaciji uslov (i = x2; i <= x1;
	// i++)
	if ((x1 > x2) || (y1 > y2)) {
		swap (x1, x2);
		swap (y1, y2);
	}

	// Bar jedan par clanova koordinata mora biti isti
	if ((x1 != x2) & (y1 != y2)) {
		cout << "Pogresan unos koordinata!" << endl;
		system ("PAUSE");
		// Ovde moze neki exception ili nekako drugacije da se resi
	}

	//

	int i = 0;
	// Kreiranje broda

	if (!this->fieldBusy (x1, y1, x2, y2)) {
		if (y1 == y2) {

			for (i = x1; i <= x2; i++) {
				Ship ship (xId, x2 - x1);
				this->setShip (i, y1, ship);
			}

		} else {

			for (i = y1; i <= y2; i++) {
				Ship ship (xId, y2 - y1);
				this->setShip (x1, i, ship);
			}
		}
		return true;
		// cout << "Brod je kreiran!" << endl;
	} else {
		cout << "Brod nije kreiran! Vec postoji brod na tim "
			"koordinatama ili pored njih"
		     << endl;
		return false;
	}
}

void
Sea::createShipRandom (int xSize, int xId)
{
	int x1, x2, y1, y2;
	do {
		srand (time (nullptr));

		bool vertical = false;
		// Random da li ce biti horizontalan ili vertikalan brod
		if ((rand () % 12) > 5)
			vertical = true;

		x1 = rand () % 8;
		y1 = rand () % 8;

		// Provera da li su koordinate pocetka negde gde mora da bude
		// brod horizontalan/vertikalan
		if (vertical) {
			if ((x1 + xSize) > 7) {
				x2 = x1 - xSize + 1;
			} else
				x2 = x1 + xSize - 1;

			y2 = y1;
		} else {
			if ((y1 + xSize) > 7) {
				y2 = y1 - xSize + 1;
			} else
				y2 = y1 + xSize - 1;

			x2 = x1;
		}

		// cout<< "Okrecem se " << endl;
	} while (this->fieldBusy (x1, y1, x2, y2));
	this->createShip (x1, y1, x2, y2, xId);
}

// Gledamo da li su zauzeta polja, kod pod komentarom sluzi kao pomoc pri
// testiranju
auto
Sea::fieldBusy (int x1, int y1, int x2, int y2) -> bool
{
	int i1, i2, j1, j2;

	if (x1 > x2) {
		int pom = x1;
		x1 = x2;
		x2 = pom;
	}

	if (y1 > y2) {
		int pom = y1;
		y1 = y2;
		y2 = pom;
	}

	// Mora da proverimo da li se brod nalazi na ivici mora.  Mora vaziti
	// x1 < x2 i y1 < y2
	if (x1 - 1 < 0)
		i1 = x1;
	else
		i1 = x1 - 1;

	if (x2 + 1 > 7)
		i2 = x2;
	else
		i2 = x2 + 1;

	if (y1 - 1 < 0)
		j1 = y1;
	else
		j1 = y1 - 1;

	if (y2 + 1 > 7)
		j2 = y2;
	else
		j2 = y2 + 1;

	// nakon svakog kretanja kroz petlju j1 mora da se vrati na pocetak
	int pomJ1 = j1;
	//    cout << i1 << " " << j1 << " "<< i2 << " " << j2<<endl;
	for (i1; i1 <= i2; i1++) {

		for (j1 = pomJ1; j1 <= j2; j1++) {
			if (this->getField (i1, j1).getId () != 0)
				return true;
			//     cout<< f[i1][j1].getId();
		}
		//   cout<<endl;
	}
	// cout << "Uspedno pregledano" << endl;
	return false;
}
