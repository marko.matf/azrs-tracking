/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI
*file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
      public:
	QWidget* centralWidget;
	QWidget* gridLayoutWidget;
	QGridLayout* gridLayout;
	QToolButton* field07;
	QToolButton* field02;
	QToolButton* field01;
	QToolButton* field03;
	QToolButton* field04;
	QToolButton* field06;
	QToolButton* field05;
	QToolButton* field10;
	QToolButton* field58;
	QToolButton* field60;
	QToolButton* field59;
	QToolButton* field62;
	QToolButton* field61;
	QToolButton* field64;
	QToolButton* field63;
	QToolButton* field53;
	QToolButton* field55;
	QToolButton* field50;
	QToolButton* field52;
	QToolButton* field51;
	QToolButton* field56;
	QToolButton* field54;
	QToolButton* field45;
	QToolButton* field43;
	QToolButton* field44;
	QToolButton* field42;
	QToolButton* field47;
	QToolButton* field48;
	QToolButton* field46;
	QToolButton* field11;
	QToolButton* field40;
	QToolButton* field39;
	QToolButton* field38;
	QToolButton* field30;
	QToolButton* field27;
	QToolButton* field29;
	QToolButton* field28;
	QToolButton* field31;
	QToolButton* field32;
	QToolButton* field35;
	QToolButton* field34;
	QToolButton* field12;
	QToolButton* field13;
	QToolButton* field36;
	QToolButton* field15;
	QToolButton* field18;
	QToolButton* field16;
	QToolButton* field17;
	QToolButton* field14;
	QToolButton* field37;
	QToolButton* field21;
	QToolButton* field24;
	QToolButton* field22;
	QToolButton* field23;
	QToolButton* field26;
	QToolButton* field19;
	QToolButton* field20;
	QToolButton* field08;
	QToolButton* field57;
	QToolButton* field49;
	QToolButton* field33;
	QToolButton* field25;
	QToolButton* field41;
	QToolButton* field09;
	QPushButton* pushButton;
	QLineEdit* lineEdit_x1;
	QLineEdit* lineEdit_y1;
	QLineEdit* lineEdit_x2;
	QLineEdit* lineEdit_y2;
	QLabel* label_2;
	QLabel* label_3;
	QLabel* label_4;
	QLabel* label_5;
	QTextEdit* textEdit;
	QLabel* label_6;
	QLabel* label_7;
	QPushButton* pushButton_2;
	QLineEdit* lineEditFire_x1;
	QLineEdit* lineEditFire_y1;
	QLabel* label;
	QCheckBox* checkBox;
	QLabel* label_8;
	QPushButton* pushButton_3;
	QPushButton* pushButton_4;
	QPushButton* pushButton_5;
	QPushButton* pushButton_6;
	QPushButton* pushButton_7;
	QPushButton* pushButton_8;
	QPushButton* pushButton_9;
	QMenuBar* menuBar;
	QToolBar* mainToolBar;
	QStatusBar* statusBar;

	void
	setupUi (QMainWindow* MainWindow)
	{
		if (MainWindow->objectName ().isEmpty ())
			MainWindow->setObjectName (
			    QStringLiteral ("MainWindow"));
		MainWindow->resize (585, 616);
		centralWidget = new QWidget (MainWindow);
		centralWidget->setObjectName (
		    QStringLiteral ("centralWidget"));
		gridLayoutWidget = new QWidget (centralWidget);
		gridLayoutWidget->setObjectName (
		    QStringLiteral ("gridLayoutWidget"));
		gridLayoutWidget->setGeometry (QRect (280, 20, 251, 261));
		gridLayout = new QGridLayout (gridLayoutWidget);
		gridLayout->setSpacing (0);
		gridLayout->setContentsMargins (11, 11, 11, 11);
		gridLayout->setObjectName (QStringLiteral ("gridLayout"));
		gridLayout->setContentsMargins (0, 0, 0, 0);
		field07 = new QToolButton (gridLayoutWidget);
		field07->setObjectName (QStringLiteral ("field07"));

		gridLayout->addWidget (field07, 0, 6, 1, 1);

		field02 = new QToolButton (gridLayoutWidget);
		field02->setObjectName (QStringLiteral ("field02"));

		gridLayout->addWidget (field02, 0, 1, 1, 1);

		field01 = new QToolButton (gridLayoutWidget);
		field01->setObjectName (QStringLiteral ("field01"));
		field01->setAutoFillBackground (false);
		field01->setStyleSheet (
		    QStringLiteral ("background-color: rgb(0, 85, 255);"));
		field01->setCheckable (false);
		field01->setChecked (false);

		gridLayout->addWidget (field01, 0, 0, 1, 1);

		field03 = new QToolButton (gridLayoutWidget);
		field03->setObjectName (QStringLiteral ("field03"));

		gridLayout->addWidget (field03, 0, 2, 1, 1);

		field04 = new QToolButton (gridLayoutWidget);
		field04->setObjectName (QStringLiteral ("field04"));

		gridLayout->addWidget (field04, 0, 3, 1, 1);

		field06 = new QToolButton (gridLayoutWidget);
		field06->setObjectName (QStringLiteral ("field06"));

		gridLayout->addWidget (field06, 0, 5, 1, 1);

		field05 = new QToolButton (gridLayoutWidget);
		field05->setObjectName (QStringLiteral ("field05"));

		gridLayout->addWidget (field05, 0, 4, 1, 1);

		field10 = new QToolButton (gridLayoutWidget);
		field10->setObjectName (QStringLiteral ("field10"));

		gridLayout->addWidget (field10, 1, 1, 1, 1);

		field58 = new QToolButton (gridLayoutWidget);
		field58->setObjectName (QStringLiteral ("field58"));

		gridLayout->addWidget (field58, 7, 1, 1, 1);

		field60 = new QToolButton (gridLayoutWidget);
		field60->setObjectName (QStringLiteral ("field60"));

		gridLayout->addWidget (field60, 7, 3, 1, 1);

		field59 = new QToolButton (gridLayoutWidget);
		field59->setObjectName (QStringLiteral ("field59"));

		gridLayout->addWidget (field59, 7, 2, 1, 1);

		field62 = new QToolButton (gridLayoutWidget);
		field62->setObjectName (QStringLiteral ("field62"));

		gridLayout->addWidget (field62, 7, 5, 1, 1);

		field61 = new QToolButton (gridLayoutWidget);
		field61->setObjectName (QStringLiteral ("field61"));

		gridLayout->addWidget (field61, 7, 4, 1, 1);

		field64 = new QToolButton (gridLayoutWidget);
		field64->setObjectName (QStringLiteral ("field64"));

		gridLayout->addWidget (field64, 7, 7, 1, 1);

		field63 = new QToolButton (gridLayoutWidget);
		field63->setObjectName (QStringLiteral ("field63"));

		gridLayout->addWidget (field63, 7, 6, 1, 1);

		field53 = new QToolButton (gridLayoutWidget);
		field53->setObjectName (QStringLiteral ("field53"));

		gridLayout->addWidget (field53, 6, 4, 1, 1);

		field55 = new QToolButton (gridLayoutWidget);
		field55->setObjectName (QStringLiteral ("field55"));

		gridLayout->addWidget (field55, 6, 6, 1, 1);

		field50 = new QToolButton (gridLayoutWidget);
		field50->setObjectName (QStringLiteral ("field50"));

		gridLayout->addWidget (field50, 6, 1, 1, 1);

		field52 = new QToolButton (gridLayoutWidget);
		field52->setObjectName (QStringLiteral ("field52"));

		gridLayout->addWidget (field52, 6, 3, 1, 1);

		field51 = new QToolButton (gridLayoutWidget);
		field51->setObjectName (QStringLiteral ("field51"));

		gridLayout->addWidget (field51, 6, 2, 1, 1);

		field56 = new QToolButton (gridLayoutWidget);
		field56->setObjectName (QStringLiteral ("field56"));

		gridLayout->addWidget (field56, 6, 7, 1, 1);

		field54 = new QToolButton (gridLayoutWidget);
		field54->setObjectName (QStringLiteral ("field54"));

		gridLayout->addWidget (field54, 6, 5, 1, 1);

		field45 = new QToolButton (gridLayoutWidget);
		field45->setObjectName (QStringLiteral ("field45"));

		gridLayout->addWidget (field45, 5, 4, 1, 1);

		field43 = new QToolButton (gridLayoutWidget);
		field43->setObjectName (QStringLiteral ("field43"));

		gridLayout->addWidget (field43, 5, 2, 1, 1);

		field44 = new QToolButton (gridLayoutWidget);
		field44->setObjectName (QStringLiteral ("field44"));

		gridLayout->addWidget (field44, 5, 3, 1, 1);

		field42 = new QToolButton (gridLayoutWidget);
		field42->setObjectName (QStringLiteral ("field42"));

		gridLayout->addWidget (field42, 5, 1, 1, 1);

		field47 = new QToolButton (gridLayoutWidget);
		field47->setObjectName (QStringLiteral ("field47"));

		gridLayout->addWidget (field47, 5, 6, 1, 1);

		field48 = new QToolButton (gridLayoutWidget);
		field48->setObjectName (QStringLiteral ("field48"));

		gridLayout->addWidget (field48, 5, 7, 1, 1);

		field46 = new QToolButton (gridLayoutWidget);
		field46->setObjectName (QStringLiteral ("field46"));

		gridLayout->addWidget (field46, 5, 5, 1, 1);

		field11 = new QToolButton (gridLayoutWidget);
		field11->setObjectName (QStringLiteral ("field11"));

		gridLayout->addWidget (field11, 1, 2, 1, 1);

		field40 = new QToolButton (gridLayoutWidget);
		field40->setObjectName (QStringLiteral ("field40"));

		gridLayout->addWidget (field40, 4, 7, 1, 1);

		field39 = new QToolButton (gridLayoutWidget);
		field39->setObjectName (QStringLiteral ("field39"));

		gridLayout->addWidget (field39, 4, 6, 1, 1);

		field38 = new QToolButton (gridLayoutWidget);
		field38->setObjectName (QStringLiteral ("field38"));

		gridLayout->addWidget (field38, 4, 5, 1, 1);

		field30 = new QToolButton (gridLayoutWidget);
		field30->setObjectName (QStringLiteral ("field30"));

		gridLayout->addWidget (field30, 3, 5, 1, 1);

		field27 = new QToolButton (gridLayoutWidget);
		field27->setObjectName (QStringLiteral ("field27"));

		gridLayout->addWidget (field27, 3, 2, 1, 1);

		field29 = new QToolButton (gridLayoutWidget);
		field29->setObjectName (QStringLiteral ("field29"));

		gridLayout->addWidget (field29, 3, 4, 1, 1);

		field28 = new QToolButton (gridLayoutWidget);
		field28->setObjectName (QStringLiteral ("field28"));

		gridLayout->addWidget (field28, 3, 3, 1, 1);

		field31 = new QToolButton (gridLayoutWidget);
		field31->setObjectName (QStringLiteral ("field31"));

		gridLayout->addWidget (field31, 3, 6, 1, 1);

		field32 = new QToolButton (gridLayoutWidget);
		field32->setObjectName (QStringLiteral ("field32"));

		gridLayout->addWidget (field32, 3, 7, 1, 1);

		field35 = new QToolButton (gridLayoutWidget);
		field35->setObjectName (QStringLiteral ("field35"));

		gridLayout->addWidget (field35, 4, 2, 1, 1);

		field34 = new QToolButton (gridLayoutWidget);
		field34->setObjectName (QStringLiteral ("field34"));

		gridLayout->addWidget (field34, 4, 1, 1, 1);

		field12 = new QToolButton (gridLayoutWidget);
		field12->setObjectName (QStringLiteral ("field12"));

		gridLayout->addWidget (field12, 1, 3, 1, 1);

		field13 = new QToolButton (gridLayoutWidget);
		field13->setObjectName (QStringLiteral ("field13"));

		gridLayout->addWidget (field13, 1, 4, 1, 1);

		field36 = new QToolButton (gridLayoutWidget);
		field36->setObjectName (QStringLiteral ("field36"));

		gridLayout->addWidget (field36, 4, 3, 1, 1);

		field15 = new QToolButton (gridLayoutWidget);
		field15->setObjectName (QStringLiteral ("field15"));

		gridLayout->addWidget (field15, 1, 6, 1, 1);

		field18 = new QToolButton (gridLayoutWidget);
		field18->setObjectName (QStringLiteral ("field18"));

		gridLayout->addWidget (field18, 2, 1, 1, 1);

		field16 = new QToolButton (gridLayoutWidget);
		field16->setObjectName (QStringLiteral ("field16"));

		gridLayout->addWidget (field16, 1, 7, 1, 1);

		field17 = new QToolButton (gridLayoutWidget);
		field17->setObjectName (QStringLiteral ("field17"));

		gridLayout->addWidget (field17, 2, 0, 1, 1);

		field14 = new QToolButton (gridLayoutWidget);
		field14->setObjectName (QStringLiteral ("field14"));

		gridLayout->addWidget (field14, 1, 5, 1, 1);

		field37 = new QToolButton (gridLayoutWidget);
		field37->setObjectName (QStringLiteral ("field37"));

		gridLayout->addWidget (field37, 4, 4, 1, 1);

		field21 = new QToolButton (gridLayoutWidget);
		field21->setObjectName (QStringLiteral ("field21"));

		gridLayout->addWidget (field21, 2, 4, 1, 1);

		field24 = new QToolButton (gridLayoutWidget);
		field24->setObjectName (QStringLiteral ("field24"));

		gridLayout->addWidget (field24, 2, 7, 1, 1);

		field22 = new QToolButton (gridLayoutWidget);
		field22->setObjectName (QStringLiteral ("field22"));

		gridLayout->addWidget (field22, 2, 5, 1, 1);

		field23 = new QToolButton (gridLayoutWidget);
		field23->setObjectName (QStringLiteral ("field23"));

		gridLayout->addWidget (field23, 2, 6, 1, 1);

		field26 = new QToolButton (gridLayoutWidget);
		field26->setObjectName (QStringLiteral ("field26"));

		gridLayout->addWidget (field26, 3, 1, 1, 1);

		field19 = new QToolButton (gridLayoutWidget);
		field19->setObjectName (QStringLiteral ("field19"));

		gridLayout->addWidget (field19, 2, 2, 1, 1);

		field20 = new QToolButton (gridLayoutWidget);
		field20->setObjectName (QStringLiteral ("field20"));

		gridLayout->addWidget (field20, 2, 3, 1, 1);

		field08 = new QToolButton (gridLayoutWidget);
		field08->setObjectName (QStringLiteral ("field08"));

		gridLayout->addWidget (field08, 0, 7, 1, 1);

		field57 = new QToolButton (gridLayoutWidget);
		field57->setObjectName (QStringLiteral ("field57"));

		gridLayout->addWidget (field57, 7, 0, 1, 1);

		field49 = new QToolButton (gridLayoutWidget);
		field49->setObjectName (QStringLiteral ("field49"));

		gridLayout->addWidget (field49, 6, 0, 1, 1);

		field33 = new QToolButton (gridLayoutWidget);
		field33->setObjectName (QStringLiteral ("field33"));

		gridLayout->addWidget (field33, 4, 0, 1, 1);

		field25 = new QToolButton (gridLayoutWidget);
		field25->setObjectName (QStringLiteral ("field25"));
		field25->setAutoFillBackground (false);
		field25->setCheckable (false);
		field25->setChecked (false);

		gridLayout->addWidget (field25, 3, 0, 1, 1);

		field41 = new QToolButton (gridLayoutWidget);
		field41->setObjectName (QStringLiteral ("field41"));

		gridLayout->addWidget (field41, 5, 0, 1, 1);

		field09 = new QToolButton (gridLayoutWidget);
		field09->setObjectName (QStringLiteral ("field09"));

		gridLayout->addWidget (field09, 1, 0, 1, 1);

		field02->raise ();
		field03->raise ();
		field04->raise ();
		field05->raise ();
		field06->raise ();
		field07->raise ();
		field08->raise ();
		field57->raise ();
		field49->raise ();
		field41->raise ();
		field33->raise ();
		field25->raise ();
		field17->raise ();
		field09->raise ();
		field10->raise ();
		field11->raise ();
		field12->raise ();
		field13->raise ();
		field14->raise ();
		field15->raise ();
		field16->raise ();
		field18->raise ();
		field19->raise ();
		field20->raise ();
		field21->raise ();
		field22->raise ();
		field23->raise ();
		field24->raise ();
		field26->raise ();
		field27->raise ();
		field28->raise ();
		field29->raise ();
		field30->raise ();
		field31->raise ();
		field32->raise ();
		field34->raise ();
		field35->raise ();
		field36->raise ();
		field37->raise ();
		field38->raise ();
		field39->raise ();
		field40->raise ();
		field42->raise ();
		field43->raise ();
		field44->raise ();
		field45->raise ();
		field46->raise ();
		field47->raise ();
		field48->raise ();
		field50->raise ();
		field51->raise ();
		field52->raise ();
		field53->raise ();
		field54->raise ();
		field55->raise ();
		field56->raise ();
		field58->raise ();
		field59->raise ();
		field60->raise ();
		field61->raise ();
		field62->raise ();
		field63->raise ();
		field64->raise ();
		field01->raise ();
		pushButton = new QPushButton (centralWidget);
		pushButton->setObjectName (QStringLiteral ("pushButton"));
		pushButton->setEnabled (true);
		pushButton->setGeometry (QRect (10, 100, 131, 21));
		pushButton->setFocusPolicy (Qt::TabFocus);
		pushButton->setToolTipDuration (1);
		pushButton->setAutoFillBackground (true);
		pushButton->setAutoDefault (true);
		pushButton->setDefault (true);
		pushButton->setFlat (false);
		lineEdit_x1 = new QLineEdit (centralWidget);
		lineEdit_x1->setObjectName (QStringLiteral ("lineEdit_x1"));
		lineEdit_x1->setGeometry (QRect (30, 20, 31, 31));
		lineEdit_y1 = new QLineEdit (centralWidget);
		lineEdit_y1->setObjectName (QStringLiteral ("lineEdit_y1"));
		lineEdit_y1->setGeometry (QRect (30, 60, 31, 31));
		lineEdit_x2 = new QLineEdit (centralWidget);
		lineEdit_x2->setObjectName (QStringLiteral ("lineEdit_x2"));
		lineEdit_x2->setGeometry (QRect (120, 20, 31, 31));
		lineEdit_y2 = new QLineEdit (centralWidget);
		lineEdit_y2->setObjectName (QStringLiteral ("lineEdit_y2"));
		lineEdit_y2->setGeometry (QRect (120, 60, 31, 31));
		label_2 = new QLabel (centralWidget);
		label_2->setObjectName (QStringLiteral ("label_2"));
		label_2->setGeometry (QRect (10, 30, 21, 16));
		label_3 = new QLabel (centralWidget);
		label_3->setObjectName (QStringLiteral ("label_3"));
		label_3->setGeometry (QRect (10, 70, 21, 16));
		label_4 = new QLabel (centralWidget);
		label_4->setObjectName (QStringLiteral ("label_4"));
		label_4->setGeometry (QRect (100, 30, 21, 16));
		label_5 = new QLabel (centralWidget);
		label_5->setObjectName (QStringLiteral ("label_5"));
		label_5->setGeometry (QRect (100, 70, 21, 16));
		textEdit = new QTextEdit (centralWidget);
		textEdit->setObjectName (QStringLiteral ("textEdit"));
		textEdit->setGeometry (QRect (10, 160, 181, 171));
		label_6 = new QLabel (centralWidget);
		label_6->setObjectName (QStringLiteral ("label_6"));
		label_6->setGeometry (QRect (300, 370, 21, 16));
		label_7 = new QLabel (centralWidget);
		label_7->setObjectName (QStringLiteral ("label_7"));
		label_7->setGeometry (QRect (300, 330, 21, 16));
		pushButton_2 = new QPushButton (centralWidget);
		pushButton_2->setObjectName (QStringLiteral ("pushButton_2"));
		pushButton_2->setEnabled (true);
		pushButton_2->setGeometry (QRect (300, 400, 131, 21));
		pushButton_2->setToolTipDuration (1);
		pushButton_2->setAutoFillBackground (true);
		lineEditFire_x1 = new QLineEdit (centralWidget);
		lineEditFire_x1->setObjectName (
		    QStringLiteral ("lineEditFire_x1"));
		lineEditFire_x1->setGeometry (QRect (320, 320, 31, 31));
		lineEditFire_y1 = new QLineEdit (centralWidget);
		lineEditFire_y1->setObjectName (
		    QStringLiteral ("lineEditFire_y1"));
		lineEditFire_y1->setGeometry (QRect (320, 360, 31, 31));
		label = new QLabel (centralWidget);
		label->setObjectName (QStringLiteral ("label"));
		label->setGeometry (QRect (10, 330, 111, 21));
		checkBox = new QCheckBox (centralWidget);
		checkBox->setObjectName (QStringLiteral ("checkBox"));
		checkBox->setGeometry (QRect (390, 290, 91, 17));
		label_8 = new QLabel (centralWidget);
		label_8->setObjectName (QStringLiteral ("label_8"));
		label_8->setGeometry (QRect (280, 290, 101, 16));
		pushButton_3 = new QPushButton (centralWidget);
		pushButton_3->setObjectName (QStringLiteral ("pushButton_3"));
		pushButton_3->setGeometry (QRect (10, 370, 141, 27));
		pushButton_4 = new QPushButton (centralWidget);
		pushButton_4->setObjectName (QStringLiteral ("pushButton_4"));
		pushButton_4->setGeometry (QRect (10, 420, 141, 27));
		pushButton_5 = new QPushButton (centralWidget);
		pushButton_5->setObjectName (QStringLiteral ("pushButton_5"));
		pushButton_5->setGeometry (QRect (10, 470, 141, 27));
		pushButton_6 = new QPushButton (centralWidget);
		pushButton_6->setObjectName (QStringLiteral ("pushButton_6"));
		pushButton_6->setGeometry (QRect (10, 520, 141, 27));
		pushButton_7 = new QPushButton (centralWidget);
		pushButton_7->setObjectName (QStringLiteral ("pushButton_7"));
		pushButton_7->setGeometry (QRect (190, 470, 141, 27));
		pushButton_8 = new QPushButton (centralWidget);
		pushButton_8->setObjectName (QStringLiteral ("pushButton_8"));
		pushButton_8->setGeometry (QRect (190, 520, 141, 27));
		pushButton_9 = new QPushButton (centralWidget);
		pushButton_9->setObjectName (QStringLiteral ("pushButton_9"));
		pushButton_9->setGeometry (QRect (190, 370, 51, 27));
		MainWindow->setCentralWidget (centralWidget);
		menuBar = new QMenuBar (MainWindow);
		menuBar->setObjectName (QStringLiteral ("menuBar"));
		menuBar->setGeometry (QRect (0, 0, 585, 21));
		MainWindow->setMenuBar (menuBar);
		mainToolBar = new QToolBar (MainWindow);
		mainToolBar->setObjectName (QStringLiteral ("mainToolBar"));
		MainWindow->addToolBar (Qt::TopToolBarArea, mainToolBar);
		statusBar = new QStatusBar (MainWindow);
		statusBar->setObjectName (QStringLiteral ("statusBar"));
		MainWindow->setStatusBar (statusBar);

		retranslateUi (MainWindow);

		QMetaObject::connectSlotsByName (MainWindow);
	} // setupUi

	void
	retranslateUi (QMainWindow* MainWindow)
	{
		MainWindow->setWindowTitle (
		    QApplication::translate ("MainWindow", "MainWindow", 0));
#ifndef QT_NO_ACCESSIBILITY
		MainWindow->setAccessibleName (
		    QApplication::translate ("MainWindow", "01", 0));
#endif // QT_NO_ACCESSIBILITY
		field07->setText (QString ());
		field02->setText (QString ());
		field01->setText (QString ());
		field03->setText (QString ());
		field04->setText (QString ());
		field06->setText (QString ());
		field05->setText (QString ());
		field10->setText (QString ());
		field58->setText (QString ());
		field60->setText (QString ());
		field59->setText (QString ());
		field62->setText (QString ());
		field61->setText (QString ());
		field64->setText (QString ());
		field63->setText (QString ());
		field53->setText (QString ());
		field55->setText (QString ());
		field50->setText (QString ());
		field52->setText (QString ());
		field51->setText (QString ());
		field56->setText (QString ());
		field54->setText (QString ());
		field45->setText (QString ());
		field43->setText (QString ());
		field44->setText (QString ());
		field42->setText (QString ());
		field47->setText (QString ());
		field48->setText (QString ());
		field46->setText (QString ());
		field11->setText (QString ());
		field40->setText (QString ());
		field39->setText (QString ());
		field38->setText (QString ());
		field30->setText (QString ());
		field27->setText (QString ());
		field29->setText (QString ());
		field28->setText (QString ());
		field31->setText (QString ());
		field32->setText (QString ());
		field35->setText (QString ());
		field34->setText (QString ());
		field12->setText (QString ());
		field13->setText (QString ());
		field36->setText (QString ());
		field15->setText (QString ());
		field18->setText (QString ());
		field16->setText (QString ());
		field17->setText (QString ());
		field14->setText (QString ());
		field37->setText (QString ());
		field21->setText (QString ());
		field24->setText (QString ());
		field22->setText (QString ());
		field23->setText (QString ());
		field26->setText (QString ());
		field19->setText (QString ());
		field20->setText (QString ());
		field08->setText (QString ());
		field57->setText (QString ());
		field49->setText (QString ());
		field33->setText (QString ());
		field25->setText (QString ());
		field41->setText (QString ());
		field09->setText (QString ());
		pushButton->setText (
		    QApplication::translate ("MainWindow", "Ubaci brod", 0));
		label_2->setText (
		    QApplication::translate ("MainWindow", "x1", 0));
		label_3->setText (
		    QApplication::translate ("MainWindow", "y1", 0));
		label_4->setText (
		    QApplication::translate ("MainWindow", "x2", 0));
		label_5->setText (
		    QApplication::translate ("MainWindow", "y2", 0));
		label_6->setText (
		    QApplication::translate ("MainWindow", "y1", 0));
		label_7->setText (
		    QApplication::translate ("MainWindow", "x1", 0));
		pushButton_2->setText (
		    QApplication::translate ("MainWindow", "Pucaj!", 0));
		label->setText (
		    QApplication::translate ("MainWindow", "Test izgled", 0));
		checkBox->setText (
		    QApplication::translate ("MainWindow", "Domaci", 0));
		label_8->setText (
		    QApplication::translate ("MainWindow", "Prikaz mora:", 0));
		pushButton_3->setText (
		    QApplication::translate ("MainWindow", "Nova flota", 0));
		pushButton_4->setText (
		    QApplication::translate ("MainWindow", "Glavna D", 0));
		pushButton_5->setText (
		    QApplication::translate ("MainWindow", "Sporedna D", 0));
		pushButton_6->setText (
		    QApplication::translate ("MainWindow", "Rotiraj", 0));
		pushButton_7->setText (
		    QApplication::translate ("MainWindow", "Horizontala", 0));
		pushButton_8->setText (
		    QApplication::translate ("MainWindow", "Vertikala", 0));
		pushButton_9->setText (
		    QApplication::translate ("MainWindow", "Del", 0));
	} // retranslateUi
};

namespace Ui
{
class MainWindow : public Ui_MainWindow
{
};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
